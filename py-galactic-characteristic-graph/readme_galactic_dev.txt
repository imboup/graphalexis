#####activate virtual environnment
$source venv/bin/activate

######after develop Graph plugin##
to install: 

$ cd py-galactic-characteristic-graph
$ pip install .

########### install py36 py37 py38: ############

$ sudo add-apt-repository ppa:deadsnakes/ppa
$ sudo apt-get update
$ sudo apt-get install python3.6 python3.7  python3.8

########### run test with py36 ############
$ tox -e py36

########### run test with py36 py37 p38 ############

$ tox

#######see html page after test ###########
$ pip install pytest-cov
$ pytest --cov=galactic_characteristic_graph --cov-report=html
$ google-chrome htmlcov/index.html &

and show the result in browser

######generate docx with sphinx

$ sudo  apt-get install python3-sphinx

$ pip install .[docs]
$ python setup.py build_sphinx	

#to visualize docs :

$ chromium-browser build/sphinx/html/index.html &
$ Using PPAPI flash



=====================================> What is work <==========================
* the graph plugin is functional and executable4
*test programs pass without errors with py37 and py 36
* tox 
===========================>what is not working for plugins and improvements<==============
* py38 does not work
* improve the test in /py-galactic-characteristic-graph/test/test_graph.py
* we can't visualize the official documentation with the order $ chromium-browser build/sphinx/html/index.html &
* develop description plugin for graph
*develop stategy plugins for graph
