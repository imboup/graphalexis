Galactic graph characteristics
==============================

This package is a filter plugin for the *py-galactic* package.

It defines 1 characteristic types in the ``graph`` group:


And one description type:


It can be used as a model to define new characteristic plugins.
