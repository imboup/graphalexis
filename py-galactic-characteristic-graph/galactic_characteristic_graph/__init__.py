"""
The :mod:`galactic_characteristic_string.characteristics` module defines classes
and functions for representing string characteristics:

* :class:`Graph` an characteristic that hold a graph characteristic.
"""
from .main import Graph, register_classes

__all__ = ("Graph", "register_classes")
