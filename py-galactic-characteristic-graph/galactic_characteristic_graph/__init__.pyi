# This Python file uses the following encoding: utf-8
from galactic.algebras.set import OrderedSet
from galactic.characteristics import Characteristic, Composite
import networkx as nx

from galactic.characteristics import Key

from typing import Iterable, Mapping, Any, Dict
class Graph(Composite):
    def __init__(
        self,
         characteristic: Characteristic, 
         alphabet: Iterable[Any]=..., 
         **kwargs: Any
    ) ->None:...
    def __call__(self, individual: Any) -> Dict[Any, Any]:...	
    @property    		
    def alphabet(self) -> OrderedSet[Any]:...
       
def register_classes() -> None:...
    
