# This Python file uses the following encoding: utf-8
from typing import Iterable, Mapping, Any, Dict
from galactic import add_yaml_constructor, add_yaml_representer
from galactic.algebras.set import OrderedSet
from galactic.characteristics import Characteristic, Composite
from galactic.characteristics import Key
import networkx as nx




class Graph(Composite):
    #@alphabet: contain the list of nodes
    #@characteristic: the characteristic inside object
    def __init__(
        self, characteristic: Characteristic, alphabet: Iterable[Any], **kwargs: Any
    ):
	    """Example
	    -------
	    >>> from galactic.characteristics import Key
	    >>> graph=Graph(Key(name="seq"),alphabet=["k","M","E", "H", "F","S"])
	    >>> graph({"seq": {'k':{'M':4,'E':1},'M':{'E':2,'H':2},'E':{'M':1,'H':3,'F':1},
	    'H':{'F': 3},'F':{'H':1},'S':{'H':1,'F':2}}})  
	    >>>{'k': {'M': 4, 'E': 1}, 'M': {'E': 2, 'H': 2}, 'E': {'M': 1, 'H': 3, 'F': 1}, 'H': {'F': 3}, 'F': {'H': 1}, 'S': {'H': 1, 'F': 2}}
	    """
	    super().__init__(characteristic, **kwargs)
	    self._alphabet = OrderedSet(alphabet)
    def __eq__(self, other: Any) -> bool:
        equal = super().__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return equal and self.alphabet == other.alphabet
    def __hash__(self) -> int:
        return hash((super().__hash__(), frozenset(self.alphabet)))

    def __call__(self, individual: Any) -> Dict[Any, Any]:
    

        indiv = self.characteristics[0](individual)
        graph = []
        to_networkx_graph = nx.Graph(indiv)
        # for each node in graph
        for node in indiv:

        # for each neighbour node of a single node
       	 for neighbour in indiv[node]:
            # if edge exists then append
            		graph.append((node, neighbour))
        
	
        
	
        
        #return to_networkx_graph
        return graph
	
	
    @property    		
    def alphabet(self) -> OrderedSet:
        # Get the alphabet	
        return self._alphabet



def register_classes() -> None:
    """
    Register all classes defined in this module.
    """ 
    add_yaml_constructor("characteristic.graph.Graph", Graph)
    add_yaml_representer(
        "characteristic.graph.Graph", Graph, "characteristics", ["alphabet"]
    )
    
    

if __name__ == "__main__":
    import doctest

    doctest.testmod()
    

"""
#graph=Graph(Key(name="seq"),alphabet=["k","M","E", "H", "F","S"])
#graph({"seq": {'k':{'M':4,'E':1},'M':{'E':2,'H':2},'E':{'M':1,'H':3,'F':1},'H':{'F':3},'F':{'H':1},'S':{'H':1,'F':2}}})

"""
