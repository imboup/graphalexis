.. py-galactic documentation master file, created by
   sphinx-quickstart on Sun Jan 14 18:35:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

=================================
Galactic graph characteristics
=================================

*py-galactic-characteristic-graph* [#logo]_
[#logocharacteristicgraph]_
is a plugin package for *py-galactic*.

.. include:: description.rst

*galactic* stands for
**GA**\ lois
**LA**\ ttices,
**C**\ oncept
**T**\ heory,
**I**\ mplicational systems and
**C**\ losures.

..  toctree::
    :maxdepth: 1
    :caption: Learning

    notebook

..  toctree::
    :maxdepth: 1
    :caption: Reference

    api.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. [#logo]
      The Galactic icon has been constructed using icons present in the
      `free star wars icons set <http://sensibleworld.com/news/free-star-wars-icons>`_
      designed by `Sensible World <http://www.iconarchive.com/artist/sensibleworld.html>`_.
      The product or characters depicted in these icons
      are © by Disney / Lucasfilm.

      It's a tribute to the french mathematician
      `Évariste Galois <https://en.wikipedia.org/wiki/Évariste_Galois>`_ who
      died at age 20 in a duel.
      In France, Concept Lattices are also called *Galois Lattices*.

.. [#logocharacteristicgraph]
        The *py-galactic-characteristic-graph* icon has been constructed using the main
        galactic icon, and the
        `Cluster, data, group, organize icon
        <https://www.iconfinder.com/icons/2205240/cluster_data_group_organize_icon>`_ designed by
        `Becris <https://www.iconfinder.com/becris>`_


