Chain
=====

.. automodule:: galactic_characteristic_string
    :members:
    :show-inheritance:
    :special-members: __call__

..  toctree::
    :hidden:
    :maxdepth: 1
