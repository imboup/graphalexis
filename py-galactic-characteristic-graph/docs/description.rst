=================
*graph* plugin
=================

.. image:: icon.svg
   :class: hidden
   :align: right
   :width: 100px
   :height: 100px
   :alt:

*py-galactic-characteristic-graph* is a plugin package for *py-galactic*.

.. role:: characteristic(literal)

It exposes 1 characteristic class known to the console
utilities by the following name:

:characteristic:`characteristic.graph.Graph` used to hold
graph values.

In the following table, the prefix ``characteristic.graph.`` must
be added to the names.

+---------------+----------------------------------------------------------+
| Name          | Arguments                                                |
+===============+==========================================================+
| ``Graph``     | | * ``characteristic``: to specify the characteristic    |
|               | |   that hold the graph                                  |
|               | | * ``alphabet``: to specify the alphabet of this        |
|               | |   characteristic                                       |
+---------------+----------------------------------------------------------+

For example, to use the graph predicate stored in a key
characteristic named "``graph``" and alphabet ``{"A","B","C"}``,
the following notation must be used in the *yaml*
explorer file:

.. code-block::yaml
    :class: admonition

    !characteristic.graph.Graph
    params:
      characteristic: !characteristic.core.Key
        params:
          name: "graph"
      alphabet: {"A","B","C"}


