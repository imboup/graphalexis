from unittest import TestCase

import yaml
from galactic import detect_plugins
from galactic.characteristics import Key

from galactic_characteristic_graph import Graph


class GraphTest(TestCase):
    def test___init__(self):
#        with self.assertRaises(ValueError):
#            _ = Graph(Key(name="graph"), alphabet={"a", "b", "c", "d", "e"})
#            _({"graph": "adck"})

        graph = Graph(Key(name="graph"), alphabet={"a", "b", "c", "d", "e"})

        self.assertEqual(
            graph.alphabet,{"a", "b", "c", "d", "e"}
            )
            

    def test___eq__(self):
        self.assertFalse(Graph(Key(name="seq"), alphabet="abcd") == 1)
        self.assertEqual(
            Graph(Key(name="seq"), alphabet={"a", "d", "c", "d", "e"}),
            Graph(Key(name="seq"), alphabet={"a", "d", "c", "e"}),
        )
        self.assertNotEqual(
            Graph(Key(name="seq1"), alphabet={"a", "d", "c", "e"}),
            Graph(Key(name="seq2"), alphabet={"a", "d", "c", "e"}),
        )
        
    
    def test___hash__(self):
        self.assertEqual(
            hash(Graph(Key(name="seq"), alphabet={"a", "d", "c", "e"})),
            hash(Graph(Key(name="seq"), alphabet={"a", "d", "c", "e"})),
        )
   
    def test_yaml(self):
        detect_plugins()
        self.assertEqual(
            "\n" + yaml.dump(Graph(Key(name="seq"), alphabet="abcd")),
            """
!characteristic.graph.Graph
arguments:
- !characteristic.core.Key
  name: seq
params:
  alphabet:
  - a
  - b
  - c
  - d
""",
        )

    
 
  
        
         

