#test_filename_input = './FGC_RefinedSet/2020-06-16_10-37-48/XML_files/001.xml'
#test_filename_output = './FGC_RefinedSet/2020-06-16_10-37-48/001.yaml'
import xmlplain
import yaml
import os.path

# Read to plain object
with open("001.xml") as inf:
  root = xmlplain.xml_to_obj(inf, strip_space=True, fold_dict=True)
# Output plain YAML
with open("001.yml", "w") as outf:
  xmlplain.obj_to_yaml(root, outf)




