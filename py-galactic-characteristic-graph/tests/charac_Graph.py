# This Python file uses the following encoding: utf-8
from typing import Any
from galactic import add_yaml_constructor, add_yaml_representer
from galactic.algebras.set import OrderedSet
from galactic.characteristics import Characteristic, Composite
import numpy
import networkx as nx
import matplotlib.pyplot as plt
from galactic.characteristics import Key

from typing import Iterable, Mapping, Any, Dict
class Graph(Composite):
   
    """	
    	@alphabet: contain the list of nodes
    	@characteristic: the characteristic inside object
    	
    """
   	
    def __init__(
        self, characteristic: Characteristic, alphabet: Iterable[Any], **kwargs: Any
    ) :
    
    
        """
       

        Example
        -------
            >>> from galactic.characteristics import Key
	    >>> graph=Graph(Key(name="seq"),alphabet=["k","M","E", "H", "F","S"])
	    >>> graph({"seq": {'k':{'M':4,'E':1},'M':{'E':2,'H':2},'E':{'M':1,'H':3,'F':1},'H':{'F':3},'F':{'H':1},'S':{'H':1,'F':2}}})
	    
		{'k': {'M': 4, 'E': 1}, 'M': {'E': 2, 'H': 2}, 'E': {'M': 1, 'H': 3, 'F': 1}, 'H': {'F': 3}, 'F': {'H': 1}, 'S': {'H': 1, 'F': 2}}

           
        """
        super().__init__(characteristic, **kwargs)
        self._alphabet = OrderedSet(alphabet)

    

    def __eq__(self, other: Any) -> bool:
    	 
        equal = super().__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return equal and self.alphabet == other.alphabet


  

    """def __call__(self, individual: Any) -> object:
    
    	individu
        graph = self.characteristics[0](individual)
        if not set(graph) <= self.graph_dico:
            raise ValueError(
                "The string contains an element which is not in the graph"
            )
        return graph
    """
    def __hash__(self) -> int:
        return hash((super().__hash__(), frozenset(self.alphabet)))

    def __call__(self, individual: Any) -> Dict[Any, Any]:
    

        indiv = self.characteristics[0](individual)
        graph=[]
        to_networkx_graph=nx.Graph(indiv)
        # for each node in graph
        for node in indiv:

        # for each neighbour node of a single node
       	 for neighbour in indiv[node]:
            # if edge exists then append
            		graph.append((node, neighbour))
        """
        # pylint: disable=isinstance-second-argument-not-valid-type
        if not isinstance(indiv, Mapping):
            raise ValueError("The given individual is not a sequence")
        for ind in indiv.values():
            if ind not in self.graph:
                raise ValueError(
                    "The sequence contains an element which is not in the alphabet"
                )

        dico = SortedDict(indiv)
        """
	
        #print("The nodes of graph:",to_networkx_graph.nodes())
        #print("The edges of graph:",to_networkx_graph.edges())

        nx.draw(to_networkx_graph)
        plt.savefig("path_graph1.png")
        plt.show()
	
	
        print("\n#################The graph result##############\n")	
        return graph





def register_classes() -> None:
   
    add_yaml_constructor("characteristic.graph.Graph", Graph)
    add_yaml_representer(
        "characteristic.graph.Graph", Graph, "characteristics", ["alphabet"]
    )
    
    
def get_graph():
	return Graph()

if __name__ == "__main__":
    import doctest

    doctest.testmod()
    






""" 
instance= Graph(Key(name="seq"), graph={
            	'k':{'M':4,'E':1},
            	
            	'M':{'E':2,'H':2},
            	
            	'E':{'M':1,'H':3,'F':1},
            	
            	'H':{'F':3},
            	
            	'F':{'H':1},
            	
            	'S':{'H':1,'F':2}
            	})
            	
          	
val= Graph({"seq":{'k':{'M':4,'E':1},'M':{'E':2,'H':2},'E':{'M':1,'H':3,'F':1},'H':{'F':3},'F':{'H':1},'S':{'H':1,'F':2}}})

print("instance",instance)
###print("val", val)


#graph=Graph(Key(name="seq"),alphabet=["k","M","E", "H", "F","S"])
#graph({"seq": {'k':{'M':4,'E':1},'M':{'E':2,'H':2},'E':{'M':1,'H':3,'F':1},'H':{'F':3},'F':{'H':1},'S':{'H':1,'F':2}}})

"""
