from galactic.characteristics import Key 
from galactic_characteristics_numerical import Number 

#comment transformer les données structuré en gephe planaire
#la classe String herite de la classe composite qui elle meme herite de characteristique
class String(Composite):

    #constructeur permettant de creer la caracteristique
   # def __init__ (self,characteristics: Characteristics, ** kwargs: Any)-> None:
   #     kwargs["transform"]=String
   #     kwargs["data"]="nan"
   #     super().__init__(characteristic, **kwargs)


   #pour que le constructeur soit appelable il faut implmenter la methode call:
    #meme si faut noter dans conversion ya deja le call
    #call de conversion appelera le transfrom

    super().__init__(characteristic,**kwargs)
    self._alphabet=OrderedSet(alphabet)

    #on convertir la donnee contenu dans lindividu en chaine de caractere
    #on va utiliser le call pour transformee la donnee d'un individu en vrai graph planaire
    def __call__(self,individual:Any) -> str:
        string=str(self.characteris[0](individual)) #on appelle la fonction standard python sur la donnee contenue dans la characteristique
        if not set(string) <= self.alphabet:
             raise: ValueError(
                 "la chaine contient un élément qui n'est pas un caractere"
             )
        return string

    #tester si deux caracteristiques sont égales(equivalent de la methode equals en java)
    def __eq__(self,other:Any) ->bool:

        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.alphabet == other.alphabet





    


########## Quelque Exemple##############

#conversion des valeurs de chaque regions en chaine
#a est une characteristic de type String

a = String(Key(name='@id') # on instancie 
print(a) # a à pour comportement d'aller chercher la characteristique qui vaut '@id'
a{{'@id':'5'}} #signifie on valeur chercher la valeur de la characteristique a, à partir de l'individu decrit par '@id' qui vaut 5 
<<<<<<< HEAD
{{'@id':5}}: est un individu

=======
{{'@id':5}}: est un individu
>>>>>>> 9edac7e289fa6e0dc6f8e459b5f8fd814c6a4626
