# This Python file uses the following encoding: utf-8

"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://gitlab.univ-lr.fr/galactic/src/characteristics/py-galactic-characteristic-graph
"""
import os

# Always prefer setuptools over distutils
from setuptools import setup


# Get the long description from the README file
HERE = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(HERE, "README.rst"), encoding="utf-8") as stream:
    LONG_DESCRIPTION = stream.read()


def add_cythonize():
    try:
        from setuptools_cythonize import get_cmdclass

        return get_cmdclass()
    except ModuleNotFoundError:
        return {}


def add_sphinx():
    try:
        from sphinx.setup_command import BuildDoc

        return {"build_sphinx": BuildDoc}
    except ModuleNotFoundError:
        return {}


AUTHOR = "The Galactic Organization"
AUTHOR_EMAIL = "contact@thegalactic.org"

setup(
    setup_requires=["pytest-runner>=5.2"],
    python_requires=">=3.6",
    cmdclass={**add_sphinx(), **add_cythonize()},
    name="py-galactic-characteristic-graph",
    # The project's description
    description="graph characteristics for the py-galactic package",
    long_description=LONG_DESCRIPTION,
    # The project's main homepage.
    url="https://gitlab.univ-lr.fr/galactic/src/characteristics/py-galactic-characteristic-graph",
    # The project's download page
    download_url="https://gitlab.univ-lr.fr/galactic/src/characteristics/py-galactic-characteristic-graph/-/archive/master/py-galactic-characteristic-graph-master.zip",
    # Author details
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    # Maintainer details
    maintainer=AUTHOR,
    maintainer_email=AUTHOR_EMAIL,
    # Choose your license
    license="BSD-3-Clause",
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   1 - Planning
        #   2 - Pre-Alpha
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        #   6 - Mature
        #   7 - Inactive
        "Development Status :: 4 - Beta",
        # Specify the OS
        "Operating System :: OS Independent",
        
        
        # Indicate who your project is intended for
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "Intended Audience :: Developers",
        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        # Natural language used
        "Natural Language :: English",
    ],
    # What does your project relate to?
    keywords="formal concept analysis",
    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=[
        "py-galactic-core>=0.3.0.dev0,<0.4.0.dev0",
        "networkx>=2.4",
    ],
    extras_require={
        "docs": [
            "sphinx>=3.1",
            "sphinx_rtd_theme>=0.4",
            "pylint>=2.4",
            "Pygments>=2.5",
            "nbsphinx>=0.7",
            "jupyter>=1.0",
            "pyyaml>=5.2",
        ],
        "test": [
            "tox>=3.14",
            "doc8",
            "graphviz>=0.13",
            "Pygments>=2.5",
            "pylint>=2.5",
            "mypy",
            "black",
            "pytest-cov",
            "nose2",
        ],
    },
    dependency_links=["https://galactic.univ-lr.fr/packages"],
    packages=["galactic_characteristic_graph"],
    include_package_data=True,
    package_data={"galactic_characteristic_graph": ["__init__.pyi", "py.typed"]},
    tests_require=["pytest"],
  
    entry_points={
        "py_galactic_extension": [
            "graph_characteristics = "
            "galactic_characteristic_graph:register_classes"
        ]
    },

)
